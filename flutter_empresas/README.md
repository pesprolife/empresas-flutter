# flutter_empresas

Projeto de teste para empresa Ioasys

## Versão Flutter

- Projeto desenvolvimento com flutter 2.8.0

## Gerenciador de Estado

- Utilizado o Mobx para gerenciador de estado.

## Gerenciador de Rotas
- Utilizado o Modular

## Gerenciamento de Requisições
- Para controlar as requisições foi adicionado uma classe "rest_client.dart", responsável por filtrar todas as requisições com e sem autenticação.

## Gerencimanento de Sessão
- Utilizado o SharedPreferences




